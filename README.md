PRTG Device Template for Generic FibreChannel devices
===========================================

This project contains all the files necessary to integrate the Generic FibreChannel
into PRTG for auto discovery and sensor creation.

The template is based on the Generic  
FC-MGMT-MIB (MIBII)	1.3.6.1.3.94.*

Download Instructions
=========================
[A zip file containing all the files in the project can be downloaded from the 
repository](https://gitlab.com/PRTG/Device-Templates/Switch-FC_Generic/-/jobs/artifacts/master/download?job=PRTGDistZip) 

Installation Instructions
=========================
Please refer to INSTALL.md

Bump
